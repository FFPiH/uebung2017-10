module STMBanking where

import Control.Concurrent.STM

data Konto = Konto
  { cents  :: Integer
  , nummer :: Integer
  }
  deriving (Show, Eq)

type Bank = [TMVar Konto]

kontoA :: Konto
kontoA = Konto 1000 1
kontoB :: Konto
kontoB = Konto 0    2
kontoC :: Konto
kontoC = Konto 2000 3

-- IMPLEMENTIEREN
debit :: Bank -> TMVar Konto -> TMVar Konto -> Integer -> STM Bank
debit b _ _ _ = return b

-- diese Transaktion überweist 1000 cent einmal reihum.
-- Danach hat jeder dasselbe Geld wie vorher - theoretisch!
testTransaction1 :: Bank -> IO Bank
testTransaction1 bank = atomically $ do
        bank <- debit bank (bank!!0) (bank!!1) 1000
        bank <- debit bank (bank!!1) (bank!!2) 1000
        debit bank (bank!!2) (bank!!0) 1000

-- Dieselbe Transaktion. Diesmal mit gelockter Bank.
testTransaction2 :: TMVar Bank -> IO (TMVar Bank)
testTransaction2 mb = atomically $ do
        bank <- takeTMVar mb
        bank <- debit bank (bank!!0) (bank!!1) 1000
        bank <- debit bank (bank!!1) (bank!!2) 1000
        bank <- debit bank (bank!!2) (bank!!0) 1000
        putTMVar mb bank
        return mb


setup :: IO Bank
setup = sequence $ newTMVarIO <$> [kontoA, kontoB, kontoC]
