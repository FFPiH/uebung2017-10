module KomplizierteFunktion
        ( komplizierteFunktion
        ) where

{-# INLINE komplizierteFunktion #-}

komplizierteFunktion x y z w = 1/exp (abs (x- 2.3))^2 * sin( (y- 2.9))^2+cos( (0.4*z- 2.6))^5 *(2*z)^2-(2*z- 2.6)^2+ (25-log(abs (w- 0.6)+1))
