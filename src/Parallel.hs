module Parallel where

import Data.List
import Data.Array
import Data.Ord
import Control.Parallel.Strategies
import Control.DeepSeq
import KomplizierteFunktion

berechnungMitEingabe :: Double -> Double -> Double -> Double -> ((Double,Double,Double,Double),Double)
berechnungMitEingabe x y z w = ((x,y,z,w),komplizierteFunktion x y z w)

-- Parallelisieren Sie nBesteEingaben mit drei verschiedenen Strategien für Listen bzw. Lazy Listen
nBesteEingaben :: Int -> Double -> [((Double,Double,Double,Double),Double)]
nBesteEingaben n d = take n $ sortOn (negate.snd) berechnungen
  where
    berechnungen = berechnungMitEingabe <$> range <*> range <*> range <*> range
    range = [(-10),(-(10-d))..10]

-- TODO: IMPLEMENTIEREN
nBesteEingabenPar1 :: Int -> Double -> [((Double,Double,Double,Double),Double)]
nBesteEingabenPar1 n d = take n $ sortOn (negate.snd) berechnungen
  where
    berechnungen = berechnungMitEingabe <$> range <*> range <*> range <*> range
    range = [(-10),(-(10-d))..10]

-- TODO: IMPLEMENTIEREN
nBesteEingabenPar2 :: Int -> Double -> [((Double,Double,Double,Double),Double)]
nBesteEingabenPar2 n d = take n $ sortOn (negate.snd) berechnungen
  where
    berechnungen = berechnungMitEingabe <$> range <*> range <*> range <*> range
    range = [(-10),(-(10-d))..10]

-- TODO: IMPLEMENTIEREN
nBesteEingabenPar3 :: Int -> Double -> [((Double,Double,Double,Double),Double)]
nBesteEingabenPar3 n d = take n $ sortOn (negate.snd) berechnungen
  where
    berechnungen = berechnungMitEingabe <$> range <*> range <*> range <*> range
    range = [(-10),(-(10-d))..10]

strategy1 = show $ nBesteEingabenPar1 10 0.5
strategy2 = show $ nBesteEingabenPar2 10 0.5
strategy3 = show $ nBesteEingabenPar3 10 0.5

