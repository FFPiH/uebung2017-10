module Main where

import qualified Control.Concurrent.Thread as Thread ( forkIO, result )
import Control.Concurrent.STM
import Control.Monad
import Data.Time.Clock

import STMBanking

main :: IO ()
main = do
        start <- getCurrentTime
        bank <- setup
        bank' <- newTMVarIO bank
        -- start 10 threads doing 10000 treansactions each
        threads <- sequence $ Thread.forkIO <$> replicate 10 (foldr1 (>=>) (replicate 10000 testTransaction2) bank')
        banks <- sequence $ (\(_,w) -> w >>= Thread.result) <$> threads
        stop <- getCurrentTime
        printMBanks banks
        print $ "Time taken: " ++ (show $ diffUTCTime stop start)

printMBanks :: [TMVar Bank] -> IO ()
printMBanks (a:as) = do
        vals <- atomically $ do
                a' <- readTMVar a
                sequence $ readTMVar <$> a'
        sequence_ $ print <$> vals
printMBanks [] = return ()
