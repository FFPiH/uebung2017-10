module Main where

import qualified Control.Concurrent.Thread as Thread ( forkIO, result )
import Control.Concurrent.MVar
import Control.Monad
import Data.Time.Clock

import Banking

main :: IO ()
main = do
        start <- getCurrentTime
        bank <- setup
        -- start 10 threads doing 10000 treansactions each
        threads <- sequence $ Thread.forkIO <$> replicate 10 (foldr1 (>=>) (replicate 10000 testTransaction1) bank)
        banks <- sequence $ (\(_,w) -> w >>= Thread.result) <$> threads
        stop <- getCurrentTime
        printBanks banks
        print $ "Time taken: " ++ (show $ diffUTCTime stop start)
printBanks :: [Bank] -> IO ()
printBanks (a:as) = do
        sequence_ $ (readMVar >=> print) <$> a
printBanks [] = return ()

